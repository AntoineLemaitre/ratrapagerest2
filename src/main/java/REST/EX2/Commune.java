package REST.EX2;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.ws.rs.core.MediaType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder = {"nbDepartement","nomCommune"})
public class Commune {

	
	public String nbDepartement;
	
	public String nomCommune;
	
	public Commune(String nbDepartement, String nomCommune) {
		super();
		this.nbDepartement = nbDepartement;
		this.nomCommune = nomCommune;
	}

	public Commune() {
		super();
	}
	//@XmlAttribute(name = "nbDepartement1")
	public String getNbDepartement() {
		return nbDepartement;
	}

	public void setNbDepartement(String nbDepartement) {
		this.nbDepartement = nbDepartement;
	}
	//@XmlAttribute(name="nomCommune1")
	public String getNomCommune() {
		return nomCommune;
	}

	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}

	@Override
	public String toString() {
		return "Commune [nbDepartement=" + nbDepartement + ", nomCommune=" + nomCommune + "]";
	}
	
	
	

}
