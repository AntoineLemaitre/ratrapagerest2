package REST.EX2;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServletId
 */
@WebServlet("/ServletId")
public class ServletId extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletId() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idString= request.getParameter("id");
		Long idLong = Long.decode(idString);
		String ID_RESPONSE="PAs de Reponse";
		if (idLong%2==0) {
			ID_RESPONSE="Cet Id est pair !";
			response.setStatus(200);
		} else {
			ID_RESPONSE="Cet Id est impair !";
			response.setStatus(404);
		}
		request.setAttribute("IdRESP",ID_RESPONSE );
		request.setAttribute("Id",idLong );
		RequestDispatcher rd = request.getRequestDispatcher("id.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
