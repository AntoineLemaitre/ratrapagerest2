package REST.EX2;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/helloICI")
public class ServletONE extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//PrintWriter writer=resp.getWriter();
		//writer.print("<h1>Hello worl with REST ! </h1>");
		//writer.print("It's "+ new Date());
		doPost(req, resp);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String codePostal= req.getParameter("cp");
		Commune postCommune = new Commune(codePostal,"Sannois");
		req.setAttribute("communeDispached", postCommune);
		RequestDispatcher rd = req.getRequestDispatcher("Commune.jsp");
		rd.forward(req, resp);
	}

	
	
}
