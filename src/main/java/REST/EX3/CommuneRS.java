package REST.EX3;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import REST.EX2.Commune;

@Path("commune")
public class CommuneRS {

	@GET @Path("hello-world")
	public String helloworld() {
		
		return "<h1>Hello World ! avec javax.ws.rs</h1>";
	}
	
	@GET @Path("{codePostal}/{nomVille}")
	//@Produces(MediaType.TEXT_XML)
	@Produces(MediaType.APPLICATION_JSON)
	public Response communewithnb( @PathParam("codePostal")  String codePostal, @PathParam("nomVille") String nomCommune) {
		
		Commune currentCommune = new Commune(codePostal,nomCommune);
		
		if (codePostal.equals("0")) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		return Response.ok(currentCommune).build();
		
	}
	
}
