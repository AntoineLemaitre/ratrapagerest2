package REST.EX3;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import REST.EX2.Commune;

public class Main {

	public static void main(String[] args) throws JAXBException {
		// TODO Auto-generated method stub
		JAXBContext jaxbContext= JAXBContext.newInstance(Commune.class);
		Marshaller marshalller=jaxbContext.createMarshaller();
		
		Commune currentCommune = new Commune("95110","paname");
		marshalller.setProperty("jaxb.formatted.output", true);
		marshalller.marshal(currentCommune, new File("xml/commune_TEST_Validé.xml"));
		System.out.println(currentCommune);
		System.out.println("Xmled");
		
		Unmarshaller unmarshaller= jaxbContext.createUnmarshaller();
		Commune XmledCommune= (Commune) unmarshaller.unmarshal(new File("xml/commune_TEST.xml"));
		System.out.println(XmledCommune);
		System.out.println("UnXmled");
		

	}

}
